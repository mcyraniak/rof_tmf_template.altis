#include "macros.hpp"

class CfgLoadouts
{
    class ROF_BLU_USMC
    {
        displayName = "USMC";
        category = "RoF BLUFOR";
        #include "blufor\usmc.hpp"
    };

    class ROF_IND_UN
    {
        displayName = "UN";
        category = "RoF INDFOR";
        #include "indifor\un.hpp"
    };
    
    class ROF_OP_VDV
    {
        displayName = "VDV";
        category = "RoF OPFOR";
        #include "opfor\vdv.hpp"
    };
};
